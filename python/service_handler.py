#!/usr/bin/python

import subprocess
import signal
import os.path
from wifi import Cell, Scheme, exceptions
from helpers import *

"""
The objects in service_handler.py handle the data from gatt.py. All configuration data is stored here as well as
acted upon in this library. When data is modified over BLE, gatt.py makes a call and update to  and object in this
library. Update for each Object in this library will cause the object to read its configuration (which can be modified
by gatt.py) and execute commands based on the interpreted values.

"""


class BaseHandler(object):
    """
    Base class for all handler objects
    """
    def __init__(self, debug=False):
        self.debug = debug
        self.defaults = {}
        self.configuration = {}

    def loadConfiguration(self):
        raise NotImplementedError

    def update(self):
        raise NotImplementedError

    def _checkFixError(self, key):
        raise NotImplementedError

    def getValue(self, key):
        if "read" not in self.defaults[key]["PERM"]:
            self.dbg("Read operation not permitted")
            return ''
        if key in self.configuration:
            self.dbg("Value requested with key: %s" % key)
            return self.configuration[key]
        else:
            self.dbg("Key does not exist: %s" % key)
            return ''

    def setValue(self, key, value):
        if "write" not in self.defaults[key]["PERM"]:
            self.dbg("Write operation not permitted")
            return None
        if key in self.configuration:
            self.dbg("Setting value with key: %s" % key)
            self.configuration[key] = dbusByteArrayToString(value)
            self.update()
        else:
            self.dbg("Key does not exist: %s" % key)

        return None

    def repairError(self, key):
        self.dbg("Failed. Setting to default.")
        self.configuration[key] = self.defaults[key]["Value"]

    def getValueDictFromDefaults(self):
        out_dict = {}
        for key in self.defaults:
            out_dict[key] = self.defaults[key]["Value"]
        return out_dict

    def dbg(self, msg):
        if self.debug:
            print(msg)


class Configuration(BaseHandler):
    """
    Configuration object. Stores information pertaining to the Config.txt file that changes how the Base Station
    operates. When initialized, Configuration tried to read Config.txt in the same path as service_handler.py.
    If there is no Config.txt file, Configuration initializes with default values.

    Configuration's default keys must line up with ConfigurationService's Characteristic names.

    """

    def __init__(self, debug=False):
        BaseHandler.__init__(self, debug=debug)
        self.dbg("Init Config")

        self.defaults = {
            'STYLE':    {'Value': '0',
                         'ID': 0x00010002,
                         'PERM': ['read', 'write']},
            'TLOC':     {'Value': 'Boston,MA,USA',
                         'ID': 0x00010003,
                         'PERM': ['read', 'write']},
            'WLOC':     {'Value': 'Boston,MA,USA',
                         'ID': 0x00010004,
                         'PERM': ['read', 'write']},
            'BDAY':     {'Value': '',
                         'ID': 0x00010005,
                         'PERM': ['read', 'write']},
            'HDAY':     {'Value': '01/01,02/14,03/17,05/25,07/04,10/31,12/25,',
                         'ID': 0x00010006,
                         'PERM': ['read', 'write']},
            'STOCK':    {'Value': '^GSPC',
                         'ID': 0x00010007,
                         'PERM': ['read', 'write']},
            'SBRIGHT':  {'Value': '20',
                         'ID': 0x00010008,
                         'PERM': ['read', 'write']},
            'NBRIGHT':  {'Value': '50',
                         'ID': 0x00010009,
                         'PERM': ['read', 'write']},
            'DBRIGHT':  {'Value': '75',
                         'ID': 0x0001000A,
                         'PERM': ['read', 'write']},
            'DIMMIN':   {'Value': '120',
                         'ID': 0x0001000B,
                         'PERM': ['read']},
            'TSLEEP':   {'Value': '20/30',
                         'ID': 0x0001000C,
                         'PERM': ['read', 'write']},
            'TWAKE':    {'Value': '07/00',
                         'ID': 0x0001000D,
                         'PERM': ['read', 'write']},
            'DEMO':     {'Value': '0',
                         'ID': 0x0001000E,
                         'PERM': ['read', 'write']},
            'NMODEEN':  {'Value': '0',
                         'ID': 0x0001000F,
                         'PERM': ['read', 'write']}
            }

        self.filename = '../../Config.txt'

        self.loadConfiguration()
        self.update()

    def loadConfiguration(self):
        if self.checkConfigFileExists():
            self.dbg("%s was found!" % self.filename)
            self.loadFromConfigFile()
        else:
            self.dbg("%s was not found." % self.filename)
            self.configuration = self.getValueDictFromDefaults()
	    self.writeToConfigFile();

    def checkConfigFileExists(self):
        self.dbg("Checking if config file exists")
        return os.path.isfile(self.filename)

    def loadFromConfigFile(self):
        self.dbg("Loading from config file")
        comment_c = '#'
        split_c = ':'

        lines = [line.rstrip('\n').replace(" ", "") for line in open(self.filename)]

        for line in lines:
            if line == '':
                self.dbg("Line is empty. Skipping.")
                continue
            if line[0] == comment_c:
                self.dbg("%s Character leads line in %s. Skipping." % (comment_c, line))
                continue
            if split_c not in line:
                self.dbg("%s Character not found in %s. Skipping." % (split_c, line))
                continue
            else:
                key, value = line.split(split_c)
                self.configuration[key] = value

        self.dbg("Configuration loaded from file.")
        self.dbg(self.configuration)

    def checkRepairConfigFileErrors(self):
        # Finds errors in config file
        self.dbg("Checking errors")
        # Get differences in dictionaries

        diff = DictDiffer(self.configuration, self.getValueDictFromDefaults())

        # Add missing keys
        missing = diff.removed()
        if len(missing) != 0:
            self.dbg("Found Missing Keys. Repairing with defaults.")
            self.dbg(missing)
            for key in missing:
                self.configuration[key] = self.defaults[key]["Value"]

        # Remove extra keys
        added = diff.added()
        if len(added) != 0:
            self.dbg("Found Extra Keys. Removing extra keys.")
            self.dbg(added)
            for key in added:
                self.configuration.pop(key, None)

        # Fixing General Issues
        for key in self.configuration:
            self._checkFixError(key)

        # Fixing Special Issues
        self.dbg("Checking NBRIGHT <= DBRIGHT")
        if int(self.configuration['NBRIGHT']) > int(self.configuration['DBRIGHT']):
            self.dbg("Failed. NBRIGHT > DBRIGHT. Setting NBRIGHT=DBRIGHT")
            self.configuration['NBRIGHT'] = self.configuration['DBRIGHT']
        else:
            self.dbg("Valid.")

        self.dbg("Repaired!")
        self.dbg(self.configuration)

    def writeToConfigFile(self):
        self.dbg("Writing to config file %s" % self.filename)
        outString = "# Configuration File\n"

        for key in self.configuration:
            outString += key + ':' + self.configuration[key] + '\n'

        with open(self.filename, 'w+') as f:
            f.write(outString)
            f.truncate()

    def update(self):
        self.checkRepairConfigFileErrors()
        self.writeToConfigFile()

    def _checkFixError(self, key):
        self.dbg("Checking %s" % key)

        if key == "STYLE":
            if not checkValidInt(self.configuration[key], range=(0, 3), debug=self.debug):
                self.repairError(key)

        elif key in ["TLOC", "WLOC", "STOCK"]:
            self.dbg("No fail criteria to check.")

        elif key in ["BDAY", "HDAY"]:
            if not checkValidDateList(self.configuration[key], debug=self.debug):
                self.repairError(key)

        elif key in ["SBRIGHT", "NBRIGHT", "DBRIGHT"]:
            if not checkValidInt(self.configuration[key], range=(0, 101), debug=self.debug):
                self.repairError(key)

        elif key == "DIMMIN":
            if not checkValidInt(self.configuration[key], range=(120, 121), debug=self.debug):
                self.repairError(key)

        elif key in ["TSLEEP", "TWAKE"]:
            if not checkValidTime(self.configuration[key]):
                self.repairError(key)

        elif key in ["DEMO", "NMODEEN"]:
            if not checkValidInt(self.configuration[key], range=(0, 2), debug=self.debug):
                self.repairError(key)
        else:
            self.dbg("INVALID KEY. KEY: %s" % key)


class Commands(BaseHandler):
    """
    Commands Object.
    """

    def __init__(self, debug=False):
        BaseHandler.__init__(self, debug=debug)
        self.dbg("Commands Config")

	serial_number = str(getSerial());

        self.defaults = {
            'REBOOT':   {'Value': '0',
                         'ID': 0x00020002,
                         'PERM': ['write']},
            'SHUTDOWN': {'Value': '0',
                         'ID': 0x00020003,
                         'PERM': ['write']},
	    'SERIAL':	{'Value': serial_number,
			 'ID': 0x00020004,
			 'PERM': ['read']}
                         }

        self.loadConfiguration()
        self.update()

    def loadConfiguration(self):
        self.configuration = self.getValueDictFromDefaults()

    def update(self):
        self.dbg("Updating")
        self.checkFixCommandErrors()
        self.executeCommands()

    def checkFixCommandErrors(self):
        self.dbg("Checking errors")
        for key in self.configuration:
            self._checkFixError(key)

    def _checkFixError(self, key):
        self.dbg("Checking %s" % key)

        if key in self.defaults:
            if not checkValidInt(self.configuration[key], range=(0, 2), debug=self.debug):
                self.repairError(key)

    def executeCommands(self):
        self.dbg("Executing Commands")
        if self.configuration["SHUTDOWN"] == '1':
            self.dbg("Shutting down.")
            self.runCommand("SHUTDOWN")
            return
        if self.configuration["REBOOT"] == '1':
            self.dbg("Rebooting.")
            self.runCommand("REBOOT")
            return

        self.dbg("No Commands to run.")

    def runCommand(self, key):

        if key == "SHUTDOWN":
            command = "/usr/bin/sudo /sbin/shutdown now"
        elif key == "REBOOT":
            command = "/usr/bin/sudo /sbin/shutdown -r now"
        else:
            self.dbg("INVALID Command Key: %s" % key)
            return

        self.dbg("Executing %s command." % key)
        process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
        output = process.communicate()[0]


class Wifi(BaseHandler):
    """
    Wifi Object
    I can't seem to connect to my phone's wifi hotspot.
    """
    def __init__(self, debug=False):
        BaseHandler.__init__(self, debug=debug)
        self.dbg("Wifi Config")

        self.defaults = {
            'SCAN':     {'Value': '1',
                         'ID': 0x00030002,
                         'PERM': ['read', 'write']},
            'LIST':     {'Value': '',
                         'ID': 0x00030003,
                         'PERM': ['read']},
            'NETWORK':  {'Value': '',
                         'ID': 0x00030004,
                         'PERM': ['read', 'write']},
            'PASS':     {'Value': '',
                         'ID': 0x00030005,
                         'PERM': ['write']},
            'CONNECT':  {'Value': '0',
                         'ID': 0x00030006,
                         'PERM': ['read', 'write']},
            'SUCCESS':  {'Value': '0',
                         'ID': 0x00030007,
                         'PERM': ['read']},
            'FUZZY':    {'Value': '0',
                         'ID': 0x00030008,
                         'PERM': ['read', 'write']}
                 }

        # Load wpa_supplicant.config info
        self.wpa_config = WPA_Supplicant()

        self.network_names = []
        self.interface = 'wlan0'
        self.base_scheme_name = 'home'
        self.scheme = None

        self.loadConfiguration()
        self.update()

    def loadConfiguration(self):
        saved_schemes = self.getAllSavedSchemes()

        config_num = len(saved_schemes)
        # Configuration 0 - no networks are saved
        self.configuration = self.getValueDictFromDefaults()

        if config_num == 0:
            self.dbg("No saved networks found.")
        # Saved Scheme Found. Let's try to connect.
        elif config_num > 0:
            self.scheme = saved_schemes[0]
            self.configuration["NETWORK"] = getSSIDfromScheme(self.scheme)
            self.configuration["CONNECT"] = '1'
            if self._tryConnectScheme():
                self.dbg("Success!")
                self.configuration["SUCCESS"] = '1'
            else:
                self.configuration["SUCCESS"] = '0'
            self.configuration["CONNECT"] = '0'

    def update(self):
        self.dbg("Updating")
        self.checkFixWifiErrors()
        self.runWifiCommands()

    def runWifiCommands(self):
        self.dbg("Checking for buffered Wifi Commands.")

        if self.configuration["SCAN"] == '1':
            self._loadFoundNetworks(end_key="^]", split_key="^_")
            self.configuration["SCAN"] = '0'
        if self.configuration["CONNECT"] == '1':
            if self.attemptConnection():
                self.dbg("CONNECTION SUCCESS")
                self.configuration["SUCCESS"] = '1'
            else:
                self.dbg("CONNECTION FAILED")
                self.configuration["SUCCESS"] = '0'
            self.configuration["CONNECT"] = '0'

    def checkFixWifiErrors(self):
        self.dbg("Checking errors")
        for key in self.configuration:
            self._checkFixError(key)

    def _checkFixError(self, key):
        self.dbg("Checking %s" % key)

        if key in ["SCAN", "CONNECT", "SUCCESS", "FUZZY"]:
            if not checkValidInt(self.configuration[key], range=(0, 2), debug=self.debug):
                self.repairError(key)
        elif key in ["LIST", "NETWORK", "PASS"]:
            self.dbg("No fail criteria to check.")
        else:
            self.dbg("INVALID KEY. KEY: %s" % key)

    def attemptConnection(self):
        # Attempts to connect to network. Returns true if connection is successful. Otherwise returns false.
        ssid = self.configuration["NETWORK"]
        passkey = self.configuration["PASS"]

        self.dbg("Attemping to connect to network %s." % ssid)

        # Check if network exists first
        cell = self.getNetworkIfFound(ssid)
        if cell is None:
            self.dbg("Could not find network.")
            return False

        # Save temporary scheme. We must save a scheme before we can connect, unfortunately.
        scheme = self.saveNetwork(cell, passkey, clear_existing=False)

        if self._tryConnectScheme(scheme=scheme, tries=2):
            self.scheme = scheme
            # Overwrite existing scheme with new scheme
            self.saveNetwork(cell, passkey, clear_existing=True)
            self.updateWpaSupplicant(cell, passkey)
            return True
        else:
            # Delete new scheme. It didn't work. Keep old scheme.
            self.dbg("Deleting attempted scheme.")
            scheme.delete()
            return False

    def _tryConnectScheme(self, scheme=None, tries=2):
        if scheme is None:
            scheme = self.scheme

        # Attempt to connect three times
        t = 0
        while t < tries:
            try:
                self.dbg("Attempting to connect to: %s" % getSSIDfromScheme(scheme))
                scheme.activate()
                return True
            except:
                self.dbg("Connection Failed. Attempt #%d" % t)
                self.dbg("Resetting Wifi.")
                self._resetWifi()
                t += 1

        self.dbg("Too many failed attempts. Aborting connection attempt.")
        return False

    def saveNetwork(self, cell, passkey, clear_existing=False):
        # Saves wifi scheme to interfaces file as 'home0', 'home1', ... 'homeN'
        if clear_existing:
            self.clearAllSavedSchemes()

        saved_name = self.getNextSchemeName()
        self.dbg("Saving [%s] network with name [%s]" % (cell.ssid, saved_name))
        scheme = Scheme.for_cell(self.interface, saved_name, cell, passkey)
        scheme.save()

        return scheme

    def updateWpaSupplicant(self, cell, passkey):
        self.wpa_config.setSSID(cell.ssid)
        self.wpa_config.setPSK(passkey)
        self.wpa_config.writeLines()

    def getAllSavedSchemes(self):
        # Wifi networks are saved as schemes with saved names as 'home0', 'home1', ... 'homeN'
        self.dbg("Finding all saved Schemes on disk...")
        schemes = []
        indx = 0
        while 1:
            scheme = Scheme.find(self.interface, self.base_scheme_name + str(indx))
            if scheme is None:
                break
            else:
                schemes.append(scheme)
            indx += 1
        self.dbg("Found %d scheme(s)." % len(schemes))
        return schemes

    def clearAllSavedSchemes(self):
        self.dbg("Clearing all saved Schemes.")
        saved_schemes = self.getAllSavedSchemes()
        self.dbg("Deleting %d Schemes." % len(saved_schemes))
        for scheme in saved_schemes:
            scheme.delete()

    def getAllVisibleNetworks(self, remove_blanks=True):
        # Get Connect-able wifi cell objects
        self.dbg("Scanning for Networks")
        cells = []
        if remove_blanks:
            for cell in self._getScanNetworks():
                if cell.ssid != '':
                    cells.append(cell)
        else:
            cells = self._getScanNetworks()

        if len(cells) == 1:
            # If not run as root, only the current connected network will be shown
            self.dbg("Only 1 network found. Did you forget to run as root?")

        return cells

    def _getScanNetworks(self, tries=2):

        for i in range(tries):
            try:
                self.dbg("Scanning for networks. Attempt #%d" % i)
                return Cell.all(self.interface)
            except exceptions.InterfaceError as e:
                self.dbg("ERROR: %s" % str(e))
                self._resetWifi()

    def _loadFoundNetworks(self, end_key='', split_key=','):
        self.network_names = getNamesFromCellArray(self.getAllVisibleNetworks(remove_blanks=True))
        self.configuration["LIST"] = ''
        for network in self.network_names:
            self.configuration["LIST"] += str(network) + split_key

        self.configuration["LIST"] += end_key

    def getNetworkIfFound(self, ssid, fuzzy_match=None):
        # Returns cell if network is found. Otherwise returns None
        self.dbg("Checking if %s is a visible network." % ssid)
        if fuzzy_match is None:
            fuzzy_match = self.getFuzzyMatch()

        networks = self.getAllVisibleNetworks(remove_blanks=True)

        if networks is None:
            self.dbg("Networks unreachable.")
            return None

        if fuzzy_match:
            min_ratio = 0.75
            network, ratio = findNetworkWithClosestName(ssid, networks, min_ratio=min_ratio)
            self.dbg("Best Network match is %f%%" % (ratio * 100))
            if ratio < min_ratio:
                self.dbg("No network match greater than %f%%" % (min_ratio * 100))
        else:
            network = None
            for n in networks:
                if n.ssid.decode("string_escape") == ssid:
                    network = n

        if network is None:
            self.dbg("%s not found." % ssid)
        else:
            self.dbg("Found!")

        return network

    def getFuzzyMatch(self):
        if self.configuration["FUZZY"] == '0':
            return False
        else:
            return True

    def findSavedSchemeFromSSID(self, ssid, debug=False):
        if debug:
            print("Looking for %s in saved schemes." % ssid)
        schemes = self.getAllSavedSchemes()
        for scheme in schemes:
            for key in scheme.options:
                if "ssid" in key:
                    if ssid == scheme.options[key]:
                        return scheme

        if debug:
            print("%s was not found in saved schemes." % ssid)
        return None

    def getNextSchemeName(self):
        saved_scheme_num = len(self.getAllSavedSchemes())
        return self.base_scheme_name + str(saved_scheme_num)

    def _resetWifi(self):
        subprocess.call(['sudo /sbin/ifdown wlan0 && sleep 10 && sudo /sbin/ifup --force wlan0'], shell=True)


if __name__ == "__main__":
    y = Wifi(debug=True)
