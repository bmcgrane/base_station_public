#!/usr/bin/python
import dbus
from difflib import SequenceMatcher as sm
import re

"""General Helpers Functions"""

from wifi import Cell, Scheme

class DictDiffer(object):
    """
    Calculate the difference between two dictionaries as:
    (1) items added
    (2) items removed
    (3) keys same in both but changed values
    (4) keys same in both and unchanged values
    """
    def __init__(self, current_dict, past_dict):
        self.current_dict, self.past_dict = current_dict, past_dict
        self.set_current, self.set_past = set(current_dict.keys()), set(past_dict.keys())
        self.intersect = self.set_current.intersection(self.set_past)

    def added(self):
        return self.set_current - self.intersect

    def removed(self):
        return self.set_past - self.intersect

    def changed(self):
        return set(o for o in self.intersect if self.past_dict[o] != self.current_dict[o])

    def unchanged(self):
        return set(o for o in self.intersect if self.past_dict[o] == self.current_dict[o])


def checkValidInt(string, range=(0, 2), debug=False):
    # Make sure range is a list of length 2
    assert(len(range) == 2)
    # Smaller value must be first
    if range[0] > range[1]:
        temp = range[0]
        range[0] = range[1]
        range[1] = temp

    # Check if we can convert to int
    try:
        string_int = int(string)
    except (ValueError, TypeError) as e:
        dbg("%s. Value: %s" % (str(e), string), debug)
        return False

    # Check if range is correct.
    if range[0] <= string_int < range[1]:
        dbg("Valid Integer.", debug)
        return True
    else:
        dbg("Value out of Range %d <= value < %d. Value: %s" % (range[0], range[1], string), debug)
        return False

def checkValidDateList(string, debug=False):
    if len(string) != 0:
        if ' ' in string:
            dbg("' ' (space) found in Datelist. Datelist: %s" % string, debug)
            return False
        if string[-1] != ',':
            dbg("Datelist does not end in a coma. Datelist: %s" % string, debug)
            return False
        dbg("Valid Datelist.", debug)
        return True
    else:
        dbg("Datelist empty, which is okay.", debug)
        return True

def checkValidTime(string, debug=False):
    if '/' not in string:
        dbg("'/' missing from Timestring. Timestring: %s" % string, debug)
        return False

    hour, minute = string.split('/')
    if not checkValidInt(hour, range=(0, 24)):
        dbg("Hour Not Valid. Hour: %s" % hour, debug)
        return False
    if not checkValidInt(minute, range=(0, 60)):
        dbg("Minute Not Valid. Minute: %s" % minute, debug)
        return False

    return True

def dbg(message, enabled):
    if enabled:
        print(message)

def strToDbusByteArray(string):
    outList = []
    for s in string:
        outList.append(dbus.Byte(s))

def dbusByteArrayToString(byte_array):
    return ''.join(map(chr, byte_array))

def getSimilarityRatio(s1, s2):
    return sm(None, s1, s2).ratio()

def closeMatch(s1, s2, min_ratio):
    return getSimilarityRatio(s1, s2) >= min_ratio

def getSerial():
  # Extract serial from cpuinfo file
  cpuserial = "0000000000000000"
  try:
    f = open('/proc/cpuinfo','r')
    for line in f:
      if line[0:6]=='Serial':
        cpuserial = line[10:26]
    f.close()
  except:
    cpuserial = "ERROR000000000"
 
  return cpuserial


"""
------------------------------------------------------
--------------------Wifi Helpers----------------------
------------------------------------------------------
"""

class WPA_Supplicant(object):
    filepath = "/etc/wpa_supplicant/wpa_supplicant.conf"
    def __init__(self):
        self.lines = self.getLines()
        self.ssid = self.getSSID()
        self.psk = self.getPSK()

    def getLines(self):
        return [line for line in open(self.filepath)]

    def writeLines(self):
        outString = ''
        for line in self.lines:
            outString += line

        with open(self.filepath, 'w+') as f:
            f.write(outString)
            f.truncate()

    def getSSID(self):
        return self._getContentAt("ssid")

    def getPSK(self):
        return self._getContentAt("psk")

    def setSSID(self, content):
        self._setContentAt("ssid", content)
        self.ssid = content

    def setPSK(self, content):
        self._setContentAt("psk", content)
        self.psk = content

    def _getContentAt(self, key):
        for line in self.lines:
            if key in line:
                return re.findall(r'"([^"]*)"', line)[0]

    def _setContentAt(self, key, content):
        for index in range(len(self.lines)):
            if key in self.lines[index]:
                self.lines[index] = '\t%s="%s"\n' % (key, content)

    def printAll(self):
        print("FULL= ", self.lines)
        print("SSID= ", self.ssid)
        print("PSK =", self.psk)

def findNetworkWithClosestName(ssid, networks, min_ratio=0.9):
    # Should return none or network
    network = None
    max_ratio = 0.0
    best = 0.0
    for n in networks:
        ratio = getSimilarityRatio(ssid, n.ssid.decode("string_escape"))
        if ratio >= min_ratio and ratio > max_ratio:
            network = n
            max_ratio = ratio
        if ratio > best:
            best = ratio

    return network, best

def getNamesFromCellArray(cellArray):
    networkNames = []
    for cell in cellArray:
        try:
            networkNames.append(cell.ssid.decode("string_escape"))
        except AttributeError:
            continue
    return networkNames

def getSSIDfromScheme(scheme):
    for key in scheme.options:
        if "ssid" in key:
            return scheme.options[key]

    return ''



