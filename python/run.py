#!/usr/bin/python

import gatt
import adv
from threading import Thread
import time 

def attempt(func, try_num):
    num = 0
    while num < try_num:
	print("Attempt %d" % num)
	if func() is None:
	    time.sleep(1)
	    num += 1
	    

def gatt_thread(name, try_num, *args):
    print("Attempting %s Init." % name)
    attempt(gatt.main, try_num)

def adv_thread(name, try_num, *args):
    print("Attempting %s Init." % name)
    attempt(adv.main, try_num)

tries = 3

try:
    gt = Thread(target=gatt_thread, args=("GATT", tries, 1)).start()
    time.sleep(5)
    at = Thread(target=adv_thread, args=("ADV", tries, 2)).start()
except Exception as ex:
    print("Error: unable to start thread. %s" % str(ex))

while 1:
    pass

gt.join()
at.join()

