#!/bin/bash

clear

echo "Updating Base Station..."

cd /home/pi/base_station_public/

# Clear local git changes and update
sudo git reset --hard
sudo git pull

echo "Starting Base Station..."

cd /home/pi/base_station_public/bin
# Run as Daemon
sudo ./base_station >log.txt 2>&1 &
# Run as normal
# sudo ./base_station

echo "Starting BLE..."

cd /home/pi/base_station_public/python
# Run BLE Scripts
sudo python run.py &
